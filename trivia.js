"use strict";

// angular.module("app").factory("TriviaService", ['$http', 'BACKEND_URL', function($http, BACKEND_URL) {
//     var BASE_URL = BACKEND_URL + '/api/feriado';

//     // return {
//     //     find: function () {
//     //         return $http.get(BASE_URL+'/getPage');
//     //     },
//     //     send: function(trivia) {
//     //         return $http.post(BASE_URL + '/' + trivia)
//     //     }
//     // }
// }]);
angular.module("app").service("TriviaService", [function () {
  this.data = {
    "trivia": {
      "title": "¿Cuánto recordás del conflicto que enfrentó al campo con el kirchnerismo?",
      "questions": [{
        "title": "¿Cuántos días duró el conflicto?",
        "answers": [
          {
            "id": 1,
            "title": "102 días"
          },
          {
            "id": 2,
            "title": "105 días"
          },
          {
            "id": 3,
            "title": "138 días"
          }
        ],
        "correct_answer": "MD5 DEL ID DE LA RESPUESTA"
      },
      {
        "title": "¿Cuántos días duró el conflicto?",
        "answers": [
          {
            "id": 1,
            "title": "102 días"
          },
          {
            "id": 2,
            "title": "105 días"
          },
          {
            "id": 3,
            "title": "138 días"
          }
        ],
        "correct_answer": "MD5 DEL ID DE LA RESPUESTA"
      },
      {
        "title": "¿Cuántos días duró el conflicto?",
        "answers": [
          {
            "id": 1,
            "title": "102 días"
          },
          {
            "id": 2,
            "title": "105 días"
          },
          {
            "id": 3,
            "title": "138 días"
          }
        ],
        "correct_answer": "MD5 DEL ID DE LA RESPUESTA"
      },
      {
        "title": "¿Cuántos días duró el conflicto?",
        "answers": [
          {
            "id": 1,
            "title": "102 días"
          },
          {
            "id": 2,
            "title": "105 días"
          },
          {
            "id": 3,
            "title": "138 días"
          }
        ],
        "correct_answer": "MD5 DEL ID DE LA RESPUESTA"
      },
      {
        "title": "¿Cuántos días duró el conflicto?",
        "answers": [
          {
            "id": 1,
            "title": "102 días"
          },
          {
            "id": 2,
            "title": "105 días"
          },
          {
            "id": 3,
            "title": "138 días"
          }
        ],
        "correct_answer": "MD5 DEL ID DE LA RESPUESTA"
      },
      {
        "title": "¿Cuántos días duró el conflicto?",
        "answers": [
          {
            "id": 1,
            "title": "102 días"
          },
          {
            "id": 2,
            "title": "105 días"
          },
          {
            "id": 3,
            "title": "138 días"
          }
        ],
        "correct_answer": "MD5 DEL ID DE LA RESPUESTA"
      },
      {
        "title": "¿Cuántos días duró el conflicto?",
        "answers": [
          {
            "id": 1,
            "title": "102 días"
          },
          {
            "id": 2,
            "title": "105 días"
          },
          {
            "id": 3,
            "title": "138 días"
          }
        ],
        "correct_answer": "MD5 DEL ID DE LA RESPUESTA"
      },
      {
        "title": "¿Cuántos días duró el conflicto?",
        "answers": [
          {
            "id": 1,
            "title": "102 días"
          },
          {
            "id": 2,
            "title": "105 días"
          },
          {
            "id": 3,
            "title": "138 días"
          }
        ],
        "correct_answer": "MD5 DEL ID DE LA RESPUESTA"
      },
      {
        "title": "¿Quién era el presidente de la Sociedad Rural Argentina?",
        "answers": [
          {
            "id": 4,
            "title": "Hugo Biolcati"
          },
          {
            "id": 5,
            "title": "Luis Miguel Etchevere"
          },
          {
            "id": 6,
            "title": "Luciano Miguens"
          }
        ],
        "correct_answer": "MD5 DEL ID DE LA RESPUESTA"
      },

      {
        "title": "¿Quién era el presidente de la Sociedad Rural Argentina?",
        "answers": [
          {
            "id": 4,
            "title": "Hugo Biolcati"
          },
          {
            "id": 5,
            "title": "Luis Miguel Etchevere"
          },
          {
            "id": 6,
            "title": "Luciano Miguens"
          }
        ],
        "correct_answer": "MD5 DEL ID DE LA RESPUESTA"
      }]
    },
    
    "date_from": "2018-10-08",
    "date_to": "2018-11-08"
  }
}]);