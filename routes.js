angular.module("app").config(['$stateProvider', '$urlRouterProvider', function ($stateProvider, $urlRouterProvider) {
    $urlRouterProvider.otherwise("/trivia/list");

    $stateProvider
        .state('trivia', {
            abstract: true,
            url: '/trivia',
            controller: 'AbstractTriviaController',
            templateUrl: 'partials/trivia/index.html',
            module: 'private'

        })
        .state('trivia.list', {
            url: '/list',
            controller: 'ListTriviaController',
            templateUrl: 'partials/trivia/list.html',
            module: 'private'
        })
    }])